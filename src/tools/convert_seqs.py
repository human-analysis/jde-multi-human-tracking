#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import os

import cv2 as cv

root = "/data/anhlt11/personal_projects/multi-object-trackings/towards-realtime-mot/raws/"


def save_img(dname, fn, i, frame):
    cv.imwrite('{}/{}_{}_{}.png'.format(
        out_dir, os.path.basename(dname),
        os.path.basename(fn).split('.')[0], i), frame)


out_dir = f"{root}/data/images"
if not os.path.exists(out_dir):
    os.makedirs(out_dir)
for dname in sorted(glob.glob(f"{root}/data/set*")):
    for fn in sorted(glob.glob('{}/*.seq'.format(dname))):
        cap = cv.VideoCapture(fn)
        i = 0
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            save_img(dname, fn, i, frame)
            i += 1
        print(fn)
